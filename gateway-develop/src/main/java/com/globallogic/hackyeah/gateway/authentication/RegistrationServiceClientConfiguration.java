package com.globallogic.hackyeah.gateway.authentication;

import org.springframework.boot.autoconfigure.web.servlet.ConditionalOnMissingFilterBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class RegistrationServiceClientConfiguration {

    private final RestTemplate restTemplate;
    private final RegistrationServiceProperties registrationServiceProperties;

    @Bean
    @ConditionalOnMissingFilterBean
    public RegistrationServiceClient registrationServiceClient() {
        return new DefaultRegistrationServiceClient(restTemplate, registrationServiceProperties);
    }
}
