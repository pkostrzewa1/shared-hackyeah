package com.globallogic.hackyeah.gateway.verification;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Validated
@ConfigurationProperties(prefix = "token")
public class JwtProperties {

    @NotBlank
    private String secretKey;
}
