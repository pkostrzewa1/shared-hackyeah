package com.globallogic.hackyeah.gateway.verification;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
class DefaultTokenValidator implements TokenValidator {

    private final JwtProperties jwtProperties;

    @Override
    public boolean isValid(final String token) {
        try {
            extractBody(token);
            return true;
        } catch (final ExpiredJwtException e) {
            log.warn("Request to parse expired JWT: {} failed: {}", token, e.getMessage());
        } catch (final UnsupportedJwtException e) {
            log.warn("Request to parse unsupported JWT: {} failed: {}", token, e.getMessage());
        } catch (final SignatureException e) {
            log.warn("Request to parse JWT with invalid signature: {} failed : {}", token, e.getMessage());
        } catch (final MalformedJwtException e) {
            log.warn("Request to parse invalid JWT: {} failed: {}", token, e.getMessage());
        } catch (final IllegalArgumentException e) {
            log.warn("Request to parse empty or null JWT: {} failed: {}", token, e.getMessage());
        }

        return false;
    }

    private void extractBody(final String token) {
        Jwts.parser()
                .setSigningKey(Keys.hmacShaKeyFor(jwtProperties.getSecretKey()
                        .getBytes()))
                .parseClaimsJws(token);
    }
}
