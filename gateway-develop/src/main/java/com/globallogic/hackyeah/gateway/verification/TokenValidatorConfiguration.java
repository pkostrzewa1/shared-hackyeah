package com.globallogic.hackyeah.gateway.verification;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TokenValidatorConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public TokenValidator tokenValidatorService(final JwtProperties jwtProperties) {
        return new DefaultTokenValidator(jwtProperties);
    }
}
