package com.globallogic.hackyeah.gateway.verification;

import com.globallogic.hackyeah.gateway.authentication.TokenAuthenticationService;
import com.globallogic.hackyeah.gateway.event.RequestInternalEventPublisher;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import reactor.core.publisher.Mono;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ApplicationTokenGatewayFilterFactory extends AbstractGatewayFilterFactory<ApplicationTokenGatewayFilterFactory.Config> {

    private static final String HEADER_KEY = "header";

    private final TokenValidator tokenValidator;
    private final JwtProperties jwtProperties;
    private final TokenAuthenticationService tokenAuthenticationService;
    private final RequestInternalEventPublisher requestInternalEventPublisher;

    public ApplicationTokenGatewayFilterFactory(final TokenValidator tokenValidator, final JwtProperties jwtProperties,
            final TokenAuthenticationService tokenAuthenticationService,
            final RequestInternalEventPublisher requestInternalEventPublisher) {
        super(Config.class);
        this.tokenValidator = tokenValidator;
        this.jwtProperties = jwtProperties;
        this.tokenAuthenticationService = tokenAuthenticationService;
        this.requestInternalEventPublisher = requestInternalEventPublisher;
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Collections.singletonList(HEADER_KEY);
    }

    @Override
    public GatewayFilter apply(final Config config) {
        return (exchange, chain) -> {
            final ServerHttpRequest request = exchange.getRequest();
            if (Objects.nonNull(request)) {
                final String uri = request.getURI()
                        .toASCIIString();
                log.info("Verifying incoming request: '{}'", uri);
                final HttpHeaders headers = request.getHeaders();
                if (Objects.nonNull(headers)) {
                    final List<String> tokens = headers.getOrDefault(config.getHeader(), Collections.emptyList());
                    if (CollectionUtils.isEmpty(tokens)) {
                        log.info("Exchange request '{}' with missing token", uri);
                        return exchangeOnMissingToken(exchange, chain);
                    } else {
                        final String token = tokens.get(0);
                        final String applicationId = getApplicationId(token);
                        final boolean isTokenValid =
                                tokenValidator.isValid(token) && tokenAuthenticationService.isApplicationRegistered(applicationId);
                        if (isTokenValid) {
                            log.info("Exchange request '{}' on successful token verification", uri);
                            return exchangeOnValidToken(exchange, chain, applicationId);
                        } else {
                            log.info("Reject request '{}' on failed token verification", uri);
                            return rejectOnError(exchange, chain);
                        }
                    }
                } else {
                    log.info("Headers not available for request: '{}'", uri);
                    return exchangeOnMissingToken(exchange, chain);
                }
            }
            return chain.filter(exchange);
        };
    }

    private Mono<Void> rejectOnError(final ServerWebExchange exchange, final GatewayFilterChain chain) {
        requestInternalEventPublisher.publishRequestRejectedEvent(exchange.getRequest());
        final ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        return response.setComplete();
    }

    private Mono<Void> exchangeOnMissingToken(final ServerWebExchange exchange, final GatewayFilterChain chain) {
        requestInternalEventPublisher.publishRequestPassedWithoutTokenEvent(exchange.getRequest());
        return chain.filter(exchange);
    }

    private Mono<Void> exchangeOnValidToken(final ServerWebExchange exchange, final GatewayFilterChain chain, final String applicationId) {
        requestInternalEventPublisher.publishRequestPassedWithTokenEvent(applicationId, exchange.getRequest());
        return chain.filter(exchange);
    }

    private String getApplicationId(final String token) {
        return Jwts.parser()
                .setSigningKey(Keys.hmacShaKeyFor(jwtProperties.getSecretKey()
                        .getBytes()))
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }

    @Getter
    @Setter
    public static class Config {
        private String header;
    }
}
