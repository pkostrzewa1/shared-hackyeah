package com.globallogic.hackyeah.gateway.event;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.server.reactive.ServerHttpRequest;
import java.net.InetSocketAddress;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
class DefaultRequestInternalEventPublisher implements RequestInternalEventPublisher {

    private final ApplicationEventPublisher publisher;

    @Override
    public void publishRequestRejectedEvent(final ServerHttpRequest request) {

        final String appId = "";
        final String sourceRemoteAddress = resolveSourceRemoteAddress(request);
        final String targetUri = resolveTargetUri(request);
        final Long timestamp = System.currentTimeMillis();

        final RequestProcessedEvent event = RequestProcessedEvent.builder()
                .appId(appId)
                .sourceRemoteAddress(sourceRemoteAddress)
                .targetUri(targetUri)
                .timestamp(timestamp)
                .result(RequestProcessedEvent.ProcessingResult.REJECTED)
                .build();

        publisher.publishEvent(event);
    }

    @Override
    public void publishRequestPassedWithTokenEvent(final String appId, final ServerHttpRequest request) {

        final String sourceRemoteAddress = resolveSourceRemoteAddress(request);
        final String targetUri = resolveTargetUri(request);
        final Long timestamp = System.currentTimeMillis();

        final RequestProcessedEvent event = RequestProcessedEvent.builder()
                .appId(appId)
                .sourceRemoteAddress(sourceRemoteAddress)
                .targetUri(targetUri)
                .timestamp(timestamp)
                .result(RequestProcessedEvent.ProcessingResult.PASSED_WITH_TOKEN)
                .build();

        publisher.publishEvent(event);
    }

    @Override
    public void publishRequestPassedWithoutTokenEvent(final ServerHttpRequest request) {

        final String sourceRemoteAddress = resolveSourceRemoteAddress(request);
        final String targetUri = resolveTargetUri(request);
        final Long timestamp = System.currentTimeMillis();

        final RequestProcessedEvent event = RequestProcessedEvent.builder()
                .sourceRemoteAddress(sourceRemoteAddress)
                .targetUri(targetUri)
                .timestamp(timestamp)
                .result(RequestProcessedEvent.ProcessingResult.PASSED_WITHOUT_TOKEN)
                .build();

        publisher.publishEvent(event);
    }

    private String resolveSourceRemoteAddress(final ServerHttpRequest request) {
        final InetSocketAddress address = request.getRemoteAddress();
        return Objects.nonNull(address) ? address.toString() : "";
    }

    private String resolveTargetUri(final ServerHttpRequest request) {
        return request.getURI()
                .toASCIIString();
    }
}
