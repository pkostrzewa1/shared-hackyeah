package com.globallogic.hackyeah.gateway;

import com.globallogic.hackyeah.gateway.authentication.RegistrationServiceProperties;
import com.globallogic.hackyeah.gateway.stream.RequestsChannel;
import com.globallogic.hackyeah.gateway.verification.JwtProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableBinding({ RequestsChannel.class })
@EnableConfigurationProperties({ JwtProperties.class, RegistrationServiceProperties.class })
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}