package com.globallogic.hackyeah.gateway.authentication;

import com.globallogic.hackyeah.gateway.verification.JwtProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class TokenAuthenticationServiceConfiguration {

    private final RegistrationServiceClient registrationServiceClient;

    @Bean
    @ConditionalOnMissingBean
    public TokenAuthenticationService tokenAuthenticationService() {
        return new DefaultTokenAuthenticationService(registrationServiceClient);
    }
}
