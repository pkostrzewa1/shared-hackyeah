package com.globallogic.hackyeah.gateway.stream;

import com.globallogic.hackyeah.gateway.event.RequestProcessedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class RequestsEventPublisher {

    private final EventPublisher publisher;
    private final RequestsChannel channel;

    @EventListener
    public void publishRequestProcessed(final RequestProcessedEvent event) {
        publisher.publish(channel.requestProcessedOutput(), event);
    }
}
