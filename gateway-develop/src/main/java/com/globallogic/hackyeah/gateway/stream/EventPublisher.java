package com.globallogic.hackyeah.gateway.stream;

import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
class EventPublisher {

    public void publish(final MessageChannel channel, final Object message) {
        Objects.requireNonNull(channel);
        Objects.requireNonNull(message);
        if (channel.send(MessageBuilder.withPayload(message)
                .build())) {
            log.info("Event sent, channel: {}", channel);
        } else {
            log.error("Event was not sent, channel: {}, event: {}", channel, message);
        }
    }
}
