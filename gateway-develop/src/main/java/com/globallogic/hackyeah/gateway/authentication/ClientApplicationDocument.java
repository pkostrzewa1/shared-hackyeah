package com.globallogic.hackyeah.gateway.authentication;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientApplicationDocument {

    private String id;
    private String applicationName;
    private String applicationUrl;
}
