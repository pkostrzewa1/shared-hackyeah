package com.globallogic.hackyeah.gateway.verification;

public interface TokenValidator {

    /**
     * @param token
     *         the token to check
     * @return <code>true</code> if given tocken is valid, <code>false otherwise</code>
     */
    boolean isValid(String token);
}
