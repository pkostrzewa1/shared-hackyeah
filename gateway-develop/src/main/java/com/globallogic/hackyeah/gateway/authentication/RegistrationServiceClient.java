package com.globallogic.hackyeah.gateway.authentication;

import java.util.Optional;

public interface RegistrationServiceClient {

    /**
     * @param id
     *      the id to look for client application document by
     * @return
     *      the optional client application document, empty if no was found.
     */
    Optional<ClientApplicationDocument> findApplicationById(final String id);
}
