package com.globallogic.hackyeah.gateway.event;

import org.springframework.http.server.reactive.ServerHttpRequest;

public interface RequestInternalEventPublisher {

    void publishRequestRejectedEvent(final ServerHttpRequest request);

    void publishRequestPassedWithoutTokenEvent(final ServerHttpRequest request);

    void publishRequestPassedWithTokenEvent(final String applicationId, final ServerHttpRequest request);
}
