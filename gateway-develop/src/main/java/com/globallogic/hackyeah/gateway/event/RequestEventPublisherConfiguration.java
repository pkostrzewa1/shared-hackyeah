package com.globallogic.hackyeah.gateway.event;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class RequestEventPublisherConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public RequestInternalEventPublisher requestEventPublisher(final ApplicationEventPublisher publisher) {
        return new DefaultRequestInternalEventPublisher(publisher);
    }
}
