package com.globallogic.hackyeah.gateway.authentication;

public interface TokenAuthenticationService {

    boolean isApplicationRegistered(final String applicationId);
}
