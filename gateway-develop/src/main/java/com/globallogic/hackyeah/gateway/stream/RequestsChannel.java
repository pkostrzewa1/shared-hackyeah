package com.globallogic.hackyeah.gateway.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface RequestsChannel {

    @Output("requestProcessedOutput")
    MessageChannel requestProcessedOutput();
}
