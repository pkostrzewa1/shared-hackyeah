package com.globallogic.hackyeah.gateway.authentication;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Validated
@ConfigurationProperties(prefix = "auth")
public class RegistrationServiceProperties {

    @NotBlank
    private String registrationServiceUrl;
}
