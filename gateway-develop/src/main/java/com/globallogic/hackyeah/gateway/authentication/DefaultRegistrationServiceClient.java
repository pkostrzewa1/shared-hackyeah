package com.globallogic.hackyeah.gateway.authentication;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
class DefaultRegistrationServiceClient implements RegistrationServiceClient {

    private final RestTemplate restTemplate;
    private final RegistrationServiceProperties properties;

    @Override
    public Optional<ClientApplicationDocument> findApplicationById(final String id) {
        final String url = properties.getRegistrationServiceUrl() + id;

        try {
            ResponseEntity<ClientApplicationDocument> entity = restTemplate.getForEntity(url, ClientApplicationDocument.class);
            return Optional.ofNullable(entity.getBody());
        } catch (final HttpClientErrorException exc) {
            if (exc.getStatusCode() == HttpStatus.NOT_FOUND) {
                log.error("Application with given id: {} was not found", id);
                return Optional.empty();
            }
            log.error("Couldn't check if application with given id: {} exists", id, exc);
            return Optional.empty();
        }
    }
}
