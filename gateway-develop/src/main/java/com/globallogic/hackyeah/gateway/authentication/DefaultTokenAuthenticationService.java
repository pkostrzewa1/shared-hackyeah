package com.globallogic.hackyeah.gateway.authentication;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class DefaultTokenAuthenticationService implements TokenAuthenticationService {

    private final RegistrationServiceClient registrationServiceClient;

    @Override
    public boolean isApplicationRegistered(final String appId) {
        return registrationServiceClient.findApplicationById(appId)
                .isPresent();
    }
}
