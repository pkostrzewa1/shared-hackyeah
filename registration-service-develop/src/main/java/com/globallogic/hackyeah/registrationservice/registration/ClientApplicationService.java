package com.globallogic.hackyeah.registrationservice.registration;

import java.util.List;
import java.util.Optional;

public interface ClientApplicationService {

    Optional<ClientApplicationDocument> findOneById(final String applicationId);

    List<ClientApplicationDocument> findAllApplications();

    ClientApplicationDocument saveApplication(final ClientApplicationDocument clientApplicationDocument);
}
