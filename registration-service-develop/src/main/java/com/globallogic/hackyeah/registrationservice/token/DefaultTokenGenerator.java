package com.globallogic.hackyeah.registrationservice.token;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Date;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class DefaultTokenGenerator implements TokenGenerator {

    private final JWTProperties jwtProperties;

    @Override
    public String generateToken(final String subject) {
        final long currentTimestamp = System.currentTimeMillis();
        return Jwts.builder()
                .setClaims(Collections.emptyMap())
                .setSubject(subject)
                .setIssuedAt(new Date(currentTimestamp))
                .setExpiration(new Date(currentTimestamp + jwtProperties.getValidityTime()
                        .toMillis()))
                .signWith(Keys.hmacShaKeyFor(jwtProperties.getPrivateKey()
                        .getBytes(StandardCharsets.UTF_8)), SignatureAlgorithm.HS256)
                .compact();
    }
}
