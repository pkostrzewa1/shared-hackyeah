package com.globallogic.hackyeah.registrationservice.registration;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DefaultClientApplicationService implements ClientApplicationService {

    private final ClientApplicationRepository clientApplicationRepository;

    @Override
    public Optional<ClientApplicationDocument> findOneById(final String applicationId) {
        return clientApplicationRepository.findById(applicationId);
    }

    @Override
    public List<ClientApplicationDocument> findAllApplications() {
        return clientApplicationRepository.findAll();
    }

    @Override
    public ClientApplicationDocument saveApplication(final ClientApplicationDocument clientApplicationDocument) {
        return clientApplicationRepository.save(clientApplicationDocument);
    }
}
