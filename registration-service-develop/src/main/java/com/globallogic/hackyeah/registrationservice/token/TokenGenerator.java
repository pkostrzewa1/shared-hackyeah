package com.globallogic.hackyeah.registrationservice.token;

public interface TokenGenerator {

    String generateToken(final String subject);
}
