package com.globallogic.hackyeah.registrationservice.registration;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ClientApplicationDocument")
@Data
@NoArgsConstructor
public class ClientApplicationDocument {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;

    private String applicationName;
    private String applicationUrl;

    @CreationTimestamp
    private LocalDateTime creationDate;

    public static ClientApplicationDocument create(final String applicationName, final String applicationUrl) {
        final ClientApplicationDocument document = new ClientApplicationDocument();
        document.applicationName = applicationName;
        document.applicationUrl = applicationUrl;
        return document;
    }
}
