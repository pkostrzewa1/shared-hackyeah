package com.globallogic.hackyeah.registrationservice.token;

public interface TokenProvider {

    String generateToken(final String applicationId);
}
