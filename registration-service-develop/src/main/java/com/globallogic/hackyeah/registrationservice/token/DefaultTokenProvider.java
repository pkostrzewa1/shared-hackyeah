package com.globallogic.hackyeah.registrationservice.token;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class DefaultTokenProvider implements TokenProvider {

    private final TokenGenerator defaultTokenGenerator;

    @Override
    public String generateToken(final String applicationId) {
        return defaultTokenGenerator.generateToken(applicationId);
    }
}
