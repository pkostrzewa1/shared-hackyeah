package com.globallogic.hackyeah.registrationservice.token;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotBlank;
import java.time.Duration;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Validated
@ConfigurationProperties("app.security.jwt")
public class JWTProperties {

    @NotBlank
    private String privateKey;

    private Duration validityTime;
}