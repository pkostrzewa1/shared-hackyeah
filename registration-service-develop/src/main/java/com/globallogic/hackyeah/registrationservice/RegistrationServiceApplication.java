package com.globallogic.hackyeah.registrationservice;

import com.globallogic.hackyeah.registrationservice.token.JWTProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({ JWTProperties.class })
public class RegistrationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegistrationServiceApplication.class, args);
    }

    //    @Bean
    //    CommandLineRunner runner(final TokenProvider tokenProvider, final JWTProperties properties) {
    //        return args -> {
    //            final String token = tokenProvider.generateToken();
    //
    //            final byte[] signingKey = properties.getPrivateKey()
    //                    .getBytes(StandardCharsets.UTF_8);
    //            final Jws<Claims> parsedToken = Jwts.parser()
    //                    .setSigningKey(signingKey)
    //                    .parseClaimsJws(token);
    //            final String subject = parsedToken.getBody()
    //                    .getSubject();
    //        };
    //    }
}
