package com.globallogic.hackyeah.registrationservice.registration;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
interface ClientApplicationRepository extends PagingAndSortingRepository<ClientApplicationDocument, String> {

    List<ClientApplicationDocument> findAll();
}
