package com.globallogic.hackyeah.registrationservice.token;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
class TokenGeneratorConfiguration {

    @Bean
    @ConditionalOnMissingBean
    TokenGenerator tokenGenerator(final JWTProperties properties) {
        return new DefaultTokenGenerator(properties);
    }
}
