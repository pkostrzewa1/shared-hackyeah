package com.globallogic.hackyeah.registrationservice.token;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
class TokenProviderConfiguration {

    @Bean
    @ConditionalOnMissingBean
    TokenProvider tokenProvider(final TokenGenerator generator) {
        return new DefaultTokenProvider(generator);
    }
}
