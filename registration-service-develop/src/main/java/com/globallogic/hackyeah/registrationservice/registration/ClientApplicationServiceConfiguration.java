package com.globallogic.hackyeah.registrationservice.registration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ClientApplicationServiceConfiguration {

    @Bean
    @ConditionalOnMissingBean
    ClientApplicationService clientApplicationService(final ClientApplicationRepository repository) {
        return new DefaultClientApplicationService(repository);
    }
}
