package com.globallogic.hackyeah.registrationservice.registration;

import com.globallogic.hackyeah.registrationservice.token.TokenProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/applications")
@RequiredArgsConstructor
class ApplicationRegistrationController {

    private final ClientApplicationService clientApplicationService;
    private final TokenProvider tokenProvider;

    @GetMapping("/{applicationId}")
    public ResponseEntity<ClientApplicationDocument> findApplicationById(@PathVariable final String applicationId) {
        final Optional<ClientApplicationDocument> oneById = clientApplicationService.findOneById(applicationId);
        return ResponseEntity.of(oneById);
    }

    @GetMapping
    public List<ClientApplicationDocument> findAll() {
        return clientApplicationService.findAllApplications();
    }

    @PostMapping
    public String registerApplication(@RequestBody final ApplicationRegistrationData registrationData) {
        final ClientApplicationDocument applicationDocument = ClientApplicationDocument.create(registrationData.getApplicationName(),
                registrationData.getApplicationUrl());
        clientApplicationService.saveApplication(applicationDocument);
        return tokenProvider.generateToken(applicationDocument.getId());
    }
}
