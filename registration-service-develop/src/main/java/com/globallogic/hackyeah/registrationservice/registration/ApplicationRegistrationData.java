package com.globallogic.hackyeah.registrationservice.registration;

import lombok.Data;

@Data
class ApplicationRegistrationData {
    private String applicationName;
    private String applicationUrl;
}
