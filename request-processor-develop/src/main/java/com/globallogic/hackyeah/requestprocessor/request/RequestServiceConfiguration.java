package com.globallogic.hackyeah.requestprocessor.request;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RequestServiceConfiguration {

    @Bean
    @ConditionalOnMissingBean
    RequestService requestService(final RequestRepository requestRepository) {
        return new DefaultRequestService(requestRepository);
    }
}
