package com.globallogic.hackyeah.requestprocessor.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface RequestsChannel {

    @Input("requestProcessedInput")
    SubscribableChannel requestProcessedInput();
}
