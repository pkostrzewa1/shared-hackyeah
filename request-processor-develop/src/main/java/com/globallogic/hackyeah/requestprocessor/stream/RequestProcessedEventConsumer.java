package com.globallogic.hackyeah.requestprocessor.stream;

import com.globallogic.hackyeah.requestprocessor.request.RequestDocument;
import com.globallogic.hackyeah.requestprocessor.request.RequestService;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
class RequestProcessedEventConsumer {

    private final RequestService requestService;

    @StreamListener("requestProcessedInput")
    void consumeRequestProcessedEvent(final @Payload RequestProcessedEvent event) {
        log.trace("Event: {}", event);
        log.info("Process request associated with applicationId = '{}' of type = '{}'", event.getAppId(), event.getResult());
        final RequestDocument document = RequestDocument.create(event.getAppId(), event.getTargetUri(), event.getSourceRemoteAddress(),
                event.getTimestamp(), event.getResult()
                        .toString());
        requestService.saveRequest(document);
    }
}
