package com.globallogic.hackyeah.requestprocessor.request;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
interface RequestRepository extends PagingAndSortingRepository<RequestDocument, String> {

    List<RequestDocument> findByApplicationId(final String applicationId);

    List<RequestDocument> findAll();
}
