package com.globallogic.hackyeah.requestprocessor.request;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/requests")
@RequiredArgsConstructor
public class RequestController {

    private final RequestService defaultRequestService;

    @GetMapping
    public List<RequestDocument> getRequests() {
        return defaultRequestService.getAllRequests();
    }

    @GetMapping("/{applicationId}")
    public List<RequestDocument> getAllRequestsByApplicationId(@PathVariable("applicationId") final String applicationId) {
        return defaultRequestService.getAllRequestsByApplicationId(applicationId);
    }
}
