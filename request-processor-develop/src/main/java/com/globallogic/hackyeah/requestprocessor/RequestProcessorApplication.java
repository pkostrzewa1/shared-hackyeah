package com.globallogic.hackyeah.requestprocessor;

import com.globallogic.hackyeah.requestprocessor.stream.RequestsChannel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableBinding({ RequestsChannel.class })
public class RequestProcessorApplication {

    public static void main(final String[] args) {
        SpringApplication.run(RequestProcessorApplication.class, args);
    }
}
