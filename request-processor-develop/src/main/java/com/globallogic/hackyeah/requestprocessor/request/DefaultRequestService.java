package com.globallogic.hackyeah.requestprocessor.request;

import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
class DefaultRequestService implements RequestService {

    private final RequestRepository requestRepository;

    @Override
    public RequestDocument saveRequest(final RequestDocument document) {
        return requestRepository.save(document);
    }

    @Override
    public List<RequestDocument> getAllRequests() {
        return requestRepository.findAll();
    }

    @Override
    public List<RequestDocument> getAllRequestsByApplicationId(final String applicationId) {
        return requestRepository.findByApplicationId(applicationId);
    }
}
