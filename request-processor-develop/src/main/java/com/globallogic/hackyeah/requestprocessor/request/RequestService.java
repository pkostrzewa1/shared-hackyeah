package com.globallogic.hackyeah.requestprocessor.request;

import java.util.List;

public interface RequestService {

    RequestDocument saveRequest(final RequestDocument document);

    List<RequestDocument> getAllRequests();

    List<RequestDocument> getAllRequestsByApplicationId(final String applicationId);
}
