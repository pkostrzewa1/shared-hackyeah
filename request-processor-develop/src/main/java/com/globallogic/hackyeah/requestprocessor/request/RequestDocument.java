package com.globallogic.hackyeah.requestprocessor.request;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "RequestDocument")
@Data()
@NoArgsConstructor
class RequestDocument {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;

    private String applicationId;
    private String targetUri;
    private String sourceIp;
    private Long timestamp;

    @CreationTimestamp
    private LocalDateTime creationDate;

    public RequestDocument(final String applicationId, final String targetUri, final String sourceIp, final Long timestamp) {
        this.applicationId = applicationId;
        this.targetUri = targetUri;
        this.sourceIp = sourceIp;
        this.timestamp = timestamp;
    }
}
