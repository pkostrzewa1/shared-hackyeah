package com.globallogic.hackyeah.requestprocessor.stream;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RequestProcessedEvent {

    private final String appId;
    private final String targetUri;
    private final String sourceRemoteAddress;
    private final Long timestamp;
    private final ProcessingResult result;

    enum ProcessingResult {
        PASSED_WITH_TOKEN, PASSED_WITHOUT_TOKEN, REJECTED
    }
}
