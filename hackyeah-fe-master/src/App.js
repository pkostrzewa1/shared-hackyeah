import React from 'react';
import './styles/main.scss';
import Home from './containers/Home/Home';
import Dashboard from './containers/Dashboard/Dashboard';
import { Switch, Route, withRouter, BrowserRouter as Router } from 'react-router-dom'
import Result from './containers/Result/Result';

const App = () => {
  return (
    <Router>
      <Routes />
    </Router>
    );
};

const Routes = withRouter(({location}) => {
  return (
    <>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/result" component={Result}/>
            <Route path="/dashboard" component={Dashboard}/>
          </Switch>
    </>
    );
});

export default App;
