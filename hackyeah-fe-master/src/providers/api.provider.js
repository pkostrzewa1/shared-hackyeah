import { API_URL } from '../config/config.api';

let API_TOKEN = null;
const getHeaders = () => {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  };
  try {
    const apiKey = API_TOKEN;
    if (apiKey) {
      headers.Authorization = `Bearer ${apiKey}`;
    }
  } catch (error) {
    return headers;
  }
  return headers;
};

class ApiDataProvider {
  setToken = apiToken => {
    API_TOKEN = apiToken;
  };

  get = async methodUri =>
    new Promise(async (resolve, reject) => {
      fetch(
        `${API_URL}/${methodUri}`,
        {
          method: 'GET',
          headers: getHeaders()
        }
      ).then(
        response => {
          if (response.status !== 200) {
            reject();
          }
          resolve(response.json());
        },
        error => reject(error.json())
      );
    });

  post = async (methodUri, data) =>
    new Promise(async (resolve, reject) => {
      fetch(
        `${API_URL}/${methodUri}`,
        {
          method: 'POST',
          headers: getHeaders(),
          body: JSON.stringify(data)
        }
      ).then(
        response => resolve(response.json()),
        error => {
          console.error(error);
          reject(error);
        }
      );
    });

  put = async (methodUri, data) =>
    new Promise(async (resolve, reject) => {
      fetch(
        `${API_URL}/${methodUri}`,
        {
          method: 'PUT',
          headers: getHeaders(),
          body: JSON.stringify(data)
        }
      ).then(
        response => resolve(response.json()),
        error => reject(error.json())
      );
    });


  delete = async methodUri =>
    new Promise(async (resolve, reject) => {
      fetch(
        `${API_URL}/${methodUri}`,
        {
          method: 'DELETE',
          headers: getHeaders()
        }
      ).then(
        response => resolve(response.json()),
        error => reject(error.json())
      );
    });
}

export default new ApiDataProvider();

