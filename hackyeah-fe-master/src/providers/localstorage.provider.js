export const storageKeys = {
  APP_TOKEN: 'appToken',
};

export const getLastCachedToken = () => JSON.parse(localStorage.getItem(storageKeys.APP_TOKEN));
export const setLastCachedToken = token => localStorage.setItem(storageKeys.APP_TOKEN, JSON.stringify(token));

