import React from 'react';
import { withRouter } from 'react-router-dom';
import styles from './Result.module.scss';
import ResultWindow from '../../components/ResultWindow/ResultWindow';
import Heading from '../../components/Heading/Heading';
import {getLastCachedToken} from '../../providers/localstorage.provider';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import ChevronLeft from '@material-ui/icons/ChevronLeft';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    color: "white"
  },
}));

const Result = ({ location, history }) => {
  const classes = useStyles();
  const data = location.state || getLastCachedToken();
  !data && history.push('/');
  const { appId, jwtToken } = data;

  const onGoBack = () => {
    history.goBack();
  };

  return (
    <>
      <IconButton className={classes.button} aria-label="delete" onClick={onGoBack}>
        <ChevronLeft />
      </IconButton>
      <Heading text={"You have generated your token: "}/>
      <div className={styles.ResultContainer}>
        {jwtToken &&
        <ResultWindow
          label="Generated token:"
          result={jwtToken}
        />}
        {appId &&
        <ResultWindow
          label="Your app id:"
          result={appId}
        />}
      </div>
    </>
  );
};

export default withRouter(Result);
