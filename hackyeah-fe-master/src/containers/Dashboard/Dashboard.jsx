import React, { useEffect, useState } from 'react';
import { faDesktop } from '@fortawesome/free-solid-svg-icons';
import ApplicationsService from '../../services/applications.service';
import Heading from '../../components/Heading/Heading';
import DataTable from '../../components/DataTable/DataTable';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { GRAFANA_URL } from '../../config/config.api';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    position: 'absolute',
    right: 0,
    top: 0
  }
}));

const redirectToGrafana = () => {
  window.open(GRAFANA_URL, '_blank');
};

const Dashboard = () => {

  const classes = useStyles();

  const [applications, setApplications] = useState([]);

  useEffect(() => {
    onLoadDashboard();
  }, []);

  const onLoadDashboard = async () => {
    const resultData = await ApplicationsService.getAllApplications();
    setApplications(resultData);
  };

  return (
    <div>
      <Heading icon={faDesktop} text={'The open data monitor'}/>
      <Button size="medium" variant="contained" className={classes.button} onClick={redirectToGrafana}>
        <OpenInNewIcon/> Show Charts
      </Button>
      <DataTable rows={applications}/>
    </div>
  );
};

export default Dashboard;
