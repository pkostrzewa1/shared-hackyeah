import React from 'react';
import Heading from '../../components/Heading/Heading';
import AddApplicationForm from './AddApplicationForm/AddApplicationForm';
import styles from "./Home.module.scss";
import ApplicationsService from '../../services/applications.service';
import { withRouter } from 'react-router-dom';
import { setLastCachedToken } from "../../providers/localstorage.provider";
import { faDesktop } from '@fortawesome/free-solid-svg-icons'

const Home = ({history}) => {
  const onAddApplicationSubmit = async ( formData ) => {
    const resultData = await ApplicationsService.addApplication( formData );
    setLastCachedToken(resultData);
    history.push("result", {...resultData});
  };

  return (
    <div className={styles.HomeContainer}>
      <Heading icon={faDesktop} text={"The open data monitor"}/>
      <AddApplicationForm
        onSubmit={onAddApplicationSubmit}
      />
    </div>
  )
};

export default withRouter(Home);
