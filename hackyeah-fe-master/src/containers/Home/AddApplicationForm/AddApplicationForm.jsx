import React, { useState } from 'react';
import styles from './AddApplicationForm.module.scss';
import { Button } from '@material-ui/core';
import Input from '../../../components/Input/Input';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';


const formKeys = {
  APP_NAME: "applicationName",
  APP_URL: "applicationUrl",
};

const useStyles = makeStyles(theme => ({
  progress: {
    marginLeft: theme.spacing(1),
  },
}));

const AddApplicationForm = ( { onSubmit }) => {


  const classes = useStyles();

  const [ applicationFormState, setApplicationFormState] = useState({
    applicationName: "",
    applicationUrl: "",
  });

  const [ isLoading, setLoading ] = useState(false);

  const handleFormValuesChangeChange = ( change, inputName ) => {
    const newValue = change.target.value;
    setApplicationFormState( {
      ...applicationFormState,
      [inputName]: newValue
    })
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    setLoading(true);
    try {
      await onSubmit(applicationFormState);
    } catch { }
    setLoading(false);
  };

  return (
    <form className={styles.ApplicationFormContainer} onSubmit={handleFormSubmit}>
      <Input
        label="Application Name"
        placeholder={"Type in your application name..."}
        variant="outlined"
        name={formKeys.APP_NAME}
        value={applicationFormState[formKeys.APP_NAME]}
        onChange={change => handleFormValuesChangeChange(change, formKeys.APP_NAME)}
        required={true}
      />
      <Input
        label="Application URL"
        placeholder={"What's your app url?"}
        variant="outlined"
        name={formKeys.APP_URL}
        value={applicationFormState[formKeys.APP_URL]}
        onChange={change => handleFormValuesChangeChange(change, formKeys.APP_URL)}
        required={true}
      />
      <Button
        className={styles.SubmitBtn}
        variant="contained"
        type="submit"
        disabled={isLoading}
      >
        GENERATE TOKEN
        { isLoading && <CircularProgress size={20} color={'primary'} className={classes.progress} /> }
      </Button>
    </form>
  )
};

export default AddApplicationForm;
