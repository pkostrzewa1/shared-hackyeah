const apiEndpoints = {
  APPLICATIONS: "applications",
  REQUESTS: "requests"
};

export default apiEndpoints;
