import { TextField } from '@material-ui/core';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import "./Input.module.scss";

const useStyles = makeStyles(theme => ({
  input: {
    margin: theme.spacing(1),
  },
}));

const Input = ( props ) => {
  const classes = useStyles();

  return (
    <>
    <TextField
      { ...props }
      variant="outlined"
      classes={{ root: classes.input }}
    />
    </>
  )
};

export default Input;
