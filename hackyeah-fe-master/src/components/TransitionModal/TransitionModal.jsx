import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import RequestsService from '../../services/requests.service';
import RequestDataTable from '../RequestDataTable/RequestDataTable';
import Button from '@material-ui/core/Button';


const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #000',
    boxShadow: theme.shadows[5],
    maxHeight: "80%",
    overflow: "auto",
    padding: theme.spacing(2, 4, 3),
  },
}));



const TransitionModal = ({applicationId}) => {

   const classes = useStyles();

   const [open, setOpen] = React.useState(false);

   const [rows, setRows] = React.useState([]);


   const handleOpen = async () => {
      const resultData = await RequestsService.getRequestsByApplicationId(applicationId);
      setRows(resultData)
      setOpen(true);
    };

   const handleClose = () => {
      setOpen(false);
    };

  return (
  <>
    <Button size="medium" variant="contained" aria-label="add" onClick={handleOpen}>
        Details
    </Button>
    <Modal
            aria-labelledby="spring-modal-title"
            aria-describedby="spring-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}>

            <Fade in={open}>
              <div className={classes.paper}>
                <RequestDataTable rows={rows}/>
              </div>
            </Fade>

    </Modal>
    </>
  )
};

export default TransitionModal;