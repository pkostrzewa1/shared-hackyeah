import React from 'react';
import styles from './Heading.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Heading = ({text, icon}) => {
  return (
    <span className={styles.HeadingContainer}>
      <h1 className={styles.title}>
        { icon && <FontAwesomeIcon icon={icon} /> }
        { text }
      </h1>
    </span>
  )
};

export default Heading;
