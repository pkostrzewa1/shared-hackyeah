import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styles from '../RequestDataTable/RequestDataTable.module.scss';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto'
  },
  table: {
    minWidth: 650
  }
}));


const RequestDataTable = ({ rows }) => {
  const classes = useStyles();

  return (
    <span className={styles.RequestDataTable}>
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Application Id</TableCell>
            <TableCell align="right">Target Uri</TableCell>
            <TableCell align="right">Source Ip</TableCell>
            <TableCell align="right">Timestamp</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.applicationId}>
              <TableCell component="th" scope="row">
                {row.applicationId}
              </TableCell>
              <TableCell align="right">{row.targetUri}</TableCell>
              <TableCell align="right">{row.sourceIp}</TableCell>
              <TableCell align="right">{row.timestamp}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    </span>
  );
};

export default RequestDataTable;