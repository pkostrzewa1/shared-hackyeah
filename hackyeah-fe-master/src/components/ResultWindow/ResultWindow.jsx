import React from 'react';
import styles from './ResultWindow.module.scss';

const ResultWindow = ( { label, result }) => {
  return (
    <div className={styles.ResultContainer}>
      <p className={styles.Label}>{label}</p>
      <div className={styles.Token}>
        { result }
      </div>
    </div>
  )
};

export default ResultWindow;
