import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styles from '../DataTable/DataTable.module.scss';
import TransitionModal from '../../components/TransitionModal/TransitionModal';

const useStyles = makeStyles(theme => ({
  root: {
    width: '60%',
    marginTop: theme.spacing(3),
    overflowX: 'auto'
  },
  appNameColumn: {
    wordBreak: 'break-word'
  }
}));


const DataTable = ({ rows }) => {
  const classes = useStyles();

  return (
    <span className={styles.DataTableContainer}>
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Application Id</TableCell>
            <TableCell className={classes.appNameColumn}>Application Name</TableCell>
            <TableCell>Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
                {row.id}
              </TableCell>
              <TableCell className={classes.appNameColumn}>{row.applicationName}</TableCell>
              <TableCell>
                <TransitionModal applicationId={row.id}/>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    </span>
  );
};

export default DataTable;