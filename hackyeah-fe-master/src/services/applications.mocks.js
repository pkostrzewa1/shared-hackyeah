export const PostConnectionsMock = {
    jwtToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    id: "D29D2-D299-D99",
};

export const AllApplicationsMock = [
    {
        id: "123",
        applicationName: "Coca Cola",
    },
    {
        id: "543634",
        applicationName: "Tymbark",
    },
    {
        id: "77236",
        applicationName: "Cafe"
    }
];