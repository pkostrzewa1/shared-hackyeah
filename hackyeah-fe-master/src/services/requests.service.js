import apiEndpoints from '../constants/endpoints.js';
import ApiDataProvider from "../providers/api.provider";
import {  AllRequestsForApplication } from './requests.mocks';
const REQUESTS_URL = apiEndpoints.REQUESTS;

class RequestsService {

  async getRequestsByApplicationId( applicationId ) {
    try {
      return await ApiDataProvider.get( `${REQUESTS_URL}/${applicationId}` );
    } catch {
      return AllRequestsForApplication;
    }
  }

}


export default new RequestsService();


