import apiEndpoints from '../constants/endpoints.js';
import ApiDataProvider from "../providers/api.provider";
import { AllApplicationsMock, PostConnectionsMock } from './applications.mocks';
const APPLICATIONS_URL = apiEndpoints.APPLICATIONS;

class ApplicationsService {

  async addApplication(appData) {
    try {
      const response = await ApiDataProvider.post( APPLICATIONS_URL, appData );
      return response;
    } catch {
      return PostConnectionsMock;
    }
  }


   async getAllApplications() {
     try {
       const response = await ApiDataProvider.get( APPLICATIONS_URL );
       return response;
     } catch {
       return AllApplicationsMock;
     }
  }

}


export default new ApplicationsService();


